# Analysis Template

template project for analysis ;)

Documentation: https://nyker510.gitlab.io/analysis-template/

## Quick Start

```bash
cp project.env .env
cp docker-compose.cpu.yml docker-compose.yml
docker-compose build
docker-compose up -d
```
