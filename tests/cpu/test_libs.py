def test_import_ml_libs():
    # ML libs
    import tensorflow
    import torch
    import torchvision
    import numpy
    import sklearn
    import scipy


def test_import_graph_libs():
    # Graph Libs
    import matplotlib.pyplot as plt
    import seaborn
    import pandas
    import plotly.express as px
    from plotly.io import write_html

    fig, ax = plt.subplots()
    ax.plot([0, 1], [0, 1])
    ax.set_xlabel('ほげほげ')
    fig.savefig('./foo.png')
    plt.close(fig)

    fig = px.bar(pandas.DataFrame({ 'hoge': [1, 2], 'foo': [2, 4]}), x='hoge', y='foo')
    fig.to_html('foo.html')

def test_import_utility():
    import PIL


def test_boosting_libs():
    import xgboost
    import lightgbm

    clf = xgboost.XGBRegressor()
    clf_lgmb = lightgbm.LGBMClassifier()


def test_mecab():
    import MeCab
    tokenizer = MeCab.Tagger('-Ochasen')
    tokenizer.parse('おはようせかい')


def test_sudachi():
    from sudachipy import tokenizer, dictionary

    tokenizer_obj = dictionary.Dictionary().create()
    s = '大阪のフロントエンドエンジニア'
    mode = tokenizer.Tokenizer.SplitMode.A
    morphemes = tokenizer_obj.tokenize(s, mode)

    for m in morphemes:
        print(m.dictionary_form(), m.part_of_speech())


def test_gensim():
    import gensim

    model = gensim.models.Word2Vec()
