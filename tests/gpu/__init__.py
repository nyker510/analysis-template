def test_basic_lib():
    import matplotlib.pyplot as plt
    import numpy as np
    import seaborn as sns
    import sklearn
    import torch
    import torchvision
    import PIL
    import optuna