def test_import_ml_libs():
    # ML libs
    import torch
    import torchvision
    import numpy
    import sklearn
    import scipy


def test_sudachi():
    from sudachipy import tokenizer, dictionary

    tokenizer_obj = dictionary.Dictionary().create()
    s = '大阪のフロントエンドエンジニア'
    mode = tokenizer.Tokenizer.SplitMode.A
    morphemes = tokenizer_obj.tokenize(s, mode)

    for m in morphemes:
        print(m.dictionary_form(), m.part_of_speech())
