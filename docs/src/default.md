# Default Settings

初期状態では以下設定で container が起動するようになっています。

## Basic

* user: `penguin`
* password: `highway`

## python

Miniconda3-4.5.12 がインストールされています。 python version は 3.7.7 です。

```bash
$ python -V
Python 3.7.7
```

## python library

基本的に必要と思われるライブラリを一括で入れています。 
`docker/requirements.txt` に記載されているライブラリは cpu・gpu で共通してインストールされているライブラリです。
keras / tensorflow については GPU 対応の関係上 CPU version にしかインストールしていませんので注意してください。

GPU versionの場合以下のライブラリがGPU対応です。

* pytorch
* XGBoost
* LightGBM

::: tip
notebooks/xgb_lgbm_gpu_speedtest.ipynb に GPU 実行時の速度ベンチマークスクリプトが入っています。 
パラメータにもよりますが XGBoost では相当の高速化が望めるようです。
:::

python library は root user でインストールされています。そのため penguin で pip install をしても追加することができません (permission error になります)。追加でライブラリをインストールしたい時には一度 docker command の User で root にユーザーを変更して pip install を実行するようにしてください。

## Jupyter

* パスワード: `dolphin`
* Port: 8888

パスワードは image 作成時に埋め込まれている為, 設定ファイルを上書きしない限り変更されません。本番環境や任意のユーザーがアクセスできる環境にこのイメージを配置して jupyter を起動する場合にはパスワードを変更することを推奨します。

## Matplotlib

デフォルトのフォントが Noto に変更されています。これは日本語をプロットした際に文字化けすることを防ぐためです。また juptyer で matplotlib を plot した時自動的に cell に output する設定が ON になっています。 

::: tip
通常 `%matplotlib inline` を記入する必要がありますが、それを省略するための設定です。
:::