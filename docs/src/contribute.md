# Contribute

開発環境のための docker-compose.yml を gpu / cpu ともに用意しています。

```bash
cp project.env .env
cp docker-compose.cpu.yml docker-compose.yml
docker-compose build
docker-compose up -d
```

If you would like to use GPU, copy `docker-compose.gpu.yml` and set `.env` to `CONTEXT=gpu`

```bash
cp project.env .env

# copy gpu file
cp docker-compose.gpu.yml docker-compose.yml

docker-compose build
docker-compose up -d
```

## Test

build 時には `/tests/<cpu|gpu>` を起点として `pytest` が実行されるようになっています。 
テストに失敗した場合 build はその時点で中断されます。 

テストは主に python package の import と基本的な動作が行えるかどうかを中心としています。 

::: tip
GPU Image のテストは gitlab ci の関係上 GPU が使えない環境で行っているという制限があることに注意してください。
:::

## change log

### 1.1

* gpu
  * cuda:10.1 に更新
  * pytorch==1.7.1
* cpu
  * keras の廃止 (tensorflow の内部に組み込まれたため削除)

### 1.0.5

* ライブラリの追加
  * ptitprin

### 1.0.4

* ライブラリの追加
  * sudachipy

#### gpu / cpu 共通

* pip install の cache directory の削除
  * image の容量が若干軽くなりました。
* conda のディレクトリを通常ユーザーの権限下になるように調整
  * pip install を root 権限で行なう必要がなくなりました。

#### その他

* notebook を rerun / mecab argument の修正

### 1.0.3

* CI の修正
* Documentation の追加 / CI で pages に自動デプロイ
* plotly の追加

### 1.0.2

* custom.css のスタイル変更 (jupyter 側の css 変更でスタイルが効かなくなっていたバグの修正)

### 1.0.1

* cpu dockerfile の working dir を `/analysis` に変更
