# Analysis Template

[https://gitlab.com/nyker510/analysis-template](https://gitlab.com/nyker510/analysis-template)

management of source code and docker images for building an analysis environment :100:

## Introduction

Analysis Template は機械学習・深層学習に必要なライブラリや設定ファイルを一元管理することで、新しいプロジェクト開始時のコストをできるだけ下げることが目的のプロジェクトです。

環境構築に Docker を使い、Docker Image を作成するために必要な Dockerfile や jupyter の設定ファイルを git で管理することで実行環境の再現性を高めています。また master merge 及び tag 作成時にその時の Dokcerfile (及びその周辺ファイル) からイメージを作成し gitlab registry に push します。 master merge 時にはその時のハッシュ値が tag を作成した場合にはそのタグ名が付与されます。 これによりユーザーは基本的なライブラリの用意に関して build することなく利用することが可能です。

## How To Use

### Quick Start

gitlab registry からイメージを pull するため docker login を実行します。


```bash
docker pull registry.gitlab.com/nyker510/analysis-template/cpu
docker run -p 8888:8888 registry.gitlab.com/nyker510/analysis-template/cpu
```

[http://localhost:8888/](http://localhost:8888/) にアクセスして jupyter の login 画面が表示されれば成功です。

### GPU Support

CPU のほかに GPU 対応をした Image を利用することもできます。

gpu と cpu ではベースとなる Image が異なります。また gpu は runtime nvidier を指定することで cuda 及び CUDNN が利用可能となっています。 pytorch や xgboost などで GPU を使いたい場合 gpu の image を利用してください。

```bash
docker pull registry.gitlab.com/nyker510/analysis-template/gpu
```

### Versioning

バージョン固定をしたい場合は tag を指定して特定の Image が pull されるように指定してください。 
現在登録されているすべてのイメージは [container_registry](https://gitlab.com/nyker510/analysis-template/container_registry) から確認できます。

## Cook Book

目的別のカスタマイズ方法について記載します。

### 追加のライブラリをインストールしたい

デフォルトのライブラリ以外を追加したい場合があると思います。その場合 analysis template の dockerimage を起点 (FROM) とした Dockerfile を記述して, そのプロジェクト用の DockerImage を作成してください。

analysis template の python 環境が root で作成されている為, USER コマンドを使って一度 root user になる必要があることに注意してください。これは package が root user で作成した path にある anaconda を利用しているためです。

```docker
FROM registry.gitlab.com/nyker510/analysis-template/cpu

USER root
COPY requirements.txt requirements.txt
RUN pip install -U pip && \
  pip install -U scikit-image==0.17.2

USER penguin
```

COPY コマンドを使えば、例えば `requirements.txt` に記載されたライブラリを追加でインストールすることも可能です。以下は `local-requirementx.txt` を一括で install しています。

```docker
FROM registry.gitlab.com/nyker510/analysis-template/cpu

USER root
COPY local-requirements.txt requirements.txt
RUN pip install -U pip && \
  pip install -U -r requirements.txt

USER penguin
```

### ローカル環境と同期させたい

開発中はローカル環境のスクリプトを docker container 内部へ同期させて開発したいことが多いです。この場合 volumes option を使います。

analysis template では初期の shell の位置が `/analysis` になっていますのでローカル環境のファイルを `/analysis` 配下に bind させると良いでしょう。`docker run` に設定をオプションとして渡す方法もありますが、かなり長くなるため `docker-compose` を用いて、設定をyml で記述することをおすすめします。

シンプルに `/analysis` にローカルの今のディレクトリのファイルを bind する yml は以下のとおりです。

```yml
version: "2.3"
services:
  jupyter:
    image: registry.gitlab.com/nyker510/analysis-template/cpu
    volumes:
      - ./:/analysis
```

機械学習プロジェクトでは大抵の場合大容量データを取り扱うことになります。そのファイルが `.py` と別のストレージなどに保存されている場合があるでしょう。例えばメインのストレージを SSD にしている場合などにソースコードと同じディレクトリに学習データなどを保存すると、すぐにSSDはいっぱいになってしまいます。この場合大容量ファイルは別途マウントしたHDDに保存したくなります。クラウドサービスなどでもたいてい大容量ファイルは別の外付けデータストレージに保存することがほとんどですから、この場合も同様です。

このような場合の時にはデータを保存したいディレクトリを `/your/dataset/dir` とすると、以下のように `/analysis` とは別にマウントすることで解決できます。(docker 内部からは `/data` にデータセットディレクトリがあるように見える)

```yml
version: "2.3"
services:
  jupyter:
    image: registry.gitlab.com/nyker510/analysis-template/cpu
    ports:
      - 8888:8888
    volumes:
      - ./:/analysis
      - /your/dataset/dir:/data
```

上記設定だと jupyter の finder から `/data` が閲覧できないためちょっと困るので `/analysis` 配下のディレクトリとして記載することが多いです。

```yml
version: "2.3"
services:
  jupyter:
    image: registry.gitlab.com/nyker510/analysis-template/cpu
    ports:
      - 8888:8888
    volumes:
      - ./:/analysis
      # analysis 配下のディレクトリなので jupyter から閲覧できる
      - /your/dataset/dir:/analysis/data
```

解決! と思うかもしれません. がこのように記述してしまうと `your/dataset/dir` の部分が環境依存になってしまいます。ようするに他のサーバーでこの yml を使おうと思うと、逐一ファイルを書き換える必要があるのです。これは若干不便ですね。

解決法のひとつに、サーバー依存の部分を `.env` に環境変数として定義して `env_file` として読み込ませる方法があります。具体的には `.env` に

```env
DATA_DIR=/your/dataset/dir
```

と書いておいて yml の方を以下のように修正します。
こうすることで環境依存の部分だけを `.env` として切り出せたのでカスタマイズが容易になりました ;)

```yml
version: "2.3"
services:
  jupyter:
    image: registry.gitlab.com/nyker510/analysis-template/cpu
    ports:
      - 8888:8888
    volumes:
      - ./:/analysis
      - ${DATA_DIR}:/analysis/data # 環境変数に変更
    env_file: # 環境変数ファイルへのパスを指定する
      - .env
```

:::tip
* これと同じ技を使うことで jupyter を開放するポートや使う image の種類, container の名前なども変更することができます. [analysis template の `docker-compose.common.yml`](https://gitlab.com/nyker510/analysis-template/-/blob/b01ad6e929648c9d4c9ba0bc638a19115c76ebe7/docker-compose.common.yml) なども参考にしてみてください。
* `.env` ファイルは雛形を `project.env` など別名にして git で追跡させて、使う人がコピーして使うような構成とすることが多いです。analysis template 自体もその構造をとっています。
:::
