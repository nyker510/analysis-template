module.exports = {
  base: '/analysis-template/',
  port: 3000,
  title: 'Analysis Template',
  description: 'Manages source code and specialized containers for building an analysis environment.',
  themeConfig: {
    sidebar: [
      '/',
      '/default',
      '/contribute',
    ],
    sidebarDepth: 3
  },
  plugins: ['@vuepress/last-updated']
}
